import fetch from 'node-fetch'
import fs from 'fs'
import jwt from 'jsonwebtoken'
import path from 'path'

var currencies = async ()=>{
  let result = await fetch('https://free.currencyconverterapi.com/api/v6/currencies');
  let currencies = await result.json()
  let arrCurrencies = [];
  for(var key in currencies.results){
   await arrCurrencies.push(currencies.results[key])
  }
  return arrCurrencies;// "done!"
}

var convertProcess = async (from,to, amount)=>{
  let getValue = await fetch(`https://free.currencyconverterapi.com/api/v6/convert?q=${from}_${to}&compact=y`);
  let value = await getValue.json();
  let valAmount = value[`${from}_${to}`].val*amount
  return {'to':to,'val':valAmount}
}

var convert = async (data)=>{
  let convertRes = [];
  if(typeof data.to == 'string'){
    let value = await convertProcess(data.from, data.to, data.amount);
    convertRes.push(value);
  }else{
    let multiConvertRes = data.to.map( async (item,idx)=>{
      let value = await convertProcess(data.from, item, data.amount);
      return value;
    })
    convertRes = await Promise.all(multiConvertRes);
  }
  return convertRes;
}

var createFile = async (data)=>{
  try { //decode token
    var decoded = await jwt.verify(data.csv, 'csvcnv');
  } catch(err) {
    return err;
  }
  let filename = Object.assign({},decoded.result);
  return filename;
}


module.exports ={
  currencies : currencies,
  convert : convert,
  createFile : createFile
}