import express from 'express'
import fs from 'fs'
import { currencies, convert, createFile } from '../function/converter' 

let router = express.Router();

router.post('/currencies',(req,res)=>{
  currencies().then(result =>{
    return res.status(200).send({result}); 
  }).catch(() =>{
    let error = {code:'ENOTFOUND',message:'ENOTFOUND'}
    return res.status(500).send({error}); 
  })
})

router.post('/convert',(req,res)=>{
  convert(req.body).then(result =>{
    return res.status(200).send({result}); 
  }).catch(() =>{
    let error = {code:'ENOTFOUND',message:'ENOTFOUND'}
    return res.status(500).send({error}); 
  });
})

router.get('/downloadcsc/:csv',(req,res)=>{
  createFile(req.params).then(result=>{
    return res.status(200).send(result); 
  }).catch(error=>{
    return res.status(500).send({error});
  });
})

export default router;

// module.exports = app;