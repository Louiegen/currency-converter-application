import http from 'http'
import fetch from 'node-fetch'

var port = process.env.PORT || 3001;

var currencies = async ()=>{
  let result = await fetch('https://free.currencyconverterapi.com/api/v6/currencies');
  let cures = await result.json()
  return cures;// "done!"
}

http.createServer((req,res)=>{
    console.log(req.headers);
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST');
    res.setHeader('Access-Control-Allow-Headers','Access-Control-Allow-Headers,Origin,Accept,X-Requested-With, Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers');
    // res.setHeader('Cache-Control', 'no-cache');
    res.setHeader("Access-Control-Allow-Origin", "*");
    // res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    req.on('error',(err)=>{
      console.error(err);
      res.statusCode = 500
      res.end();
    })

    if(req.method == 'POST' && req.url== '/currencies'){
      currencies().then(result=>{
        res.statusCode = 200
        res.end(`${JSON.stringify(result)}`);
      }).catch(err => {
        console.log();
        res.statusCode = 200
        res.end(`${JSON.stringify(err)}`)
      })
    }else{
      res.statusCode = 200;
      res.end("<h1>Hello nodejs with babel</h1>");
    }

}).listen(port, ()=>{
  console.log(`nodejs with babel server has start at port ${port} in the localhost ....`);
})

// fetch('https://free.currencyconverterapi.com/api/v6/currencies')
//   .then((response)=>{
//     console.log(response);
//   })

