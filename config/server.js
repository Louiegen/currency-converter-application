import express from 'express'
import router from '../routes/converter'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import cors from 'cors'

let port = process.env.PORT || 3001

const app = express();
// const router = express.Router();
app.use(helmet());
app.use(cors());
app.use(express.static("build"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({limit: '100mb', extended: false}));
app.use((req,res,next)=>{
  res.header('Access-Control-Allow-Methods', 'GET,POST');
  res.header('Access-Control-Allow-Headers','Access-Control-Allow-Headers,Origin,Accept,X-Requested-With, Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers');
  res.setHeader('Cache-Control', 'no-cache');
  res.header("Access-Control-Allow-Origin", "*"); //this will remove after build th fontend
  next()
})

app.post(['/currencies','/convert'],router)
app.get('/downloadcsc/:csv', router);

app.listen(port,()=>{
  console.log(`start at port ${port} in the localhost ....`);
})